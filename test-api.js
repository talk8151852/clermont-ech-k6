import { check } from 'k6';
import http from 'k6/http';

export default function () {
    const response = http.get('https://jsonplaceholder.typicode.com/posts'); //http fournit une méthode pour chaque verbe HTTP
    check(response, {
      'is status 200': (r) => r.status === 200,
    });
}